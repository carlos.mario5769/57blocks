//
//  ViewController.swift
//  Draw
//
//  Created by Carlos on 7/09/21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var drawView: DrawView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func completeButtonDidClick(_ sender: Any) {
        drawView.completePolygon()
    }
    
    @IBAction func cleanButtonDidClick(_ sender: Any) {
        drawView.cleanPolygon()
    }
    
}

