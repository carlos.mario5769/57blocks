//
//  DrawView.swift
//  Draw
//
//  Created by Carlos on 7/09/21.
//

import Foundation
import UIKit

class DrawView: UIView {

    var touch : UITouch!
    var polygonArray : [[CGPoint]] = [[CGPoint]()]
    var index = 0

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        touch = touches.first! as UITouch
        let lastPoint = touch.location(in: self)

        polygonArray[index].append(lastPoint)
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        touch = touches.first! as UITouch
        let currentPoint = touch.location(in: self)

        self.setNeedsDisplay()

        polygonArray[index].append(currentPoint)
    }

    override func draw(_ rect: CGRect) {

        if(index >= 0){
            let context = UIGraphicsGetCurrentContext()
            context!.setLineWidth(5)
            context!.setStrokeColor((UIColor.blue).cgColor)
            context!.setLineCap(.round)
            
            var j = 0
            while( j <= index ){
                if polygonArray[j].count > 0 {
                    context!.beginPath()
                    var i = 0
                    context?.move(to: polygonArray[j][0])
                    while(i < polygonArray[j].count){
                        context?.addLine(to: polygonArray[j][i])
                        i += 1
                    }
                    context!.strokePath()
                }
                j += 1
            }
        }
    }
    
    func completePolygon() {
        polygonArray[index].append(polygonArray[index][0])
        index += 1
        polygonArray.append([CGPoint]())
        self.setNeedsDisplay()
    }
    
    func cleanPolygon() {
        polygonArray = [[CGPoint]()]
        index = 0
        self.setNeedsDisplay()
    }

}
